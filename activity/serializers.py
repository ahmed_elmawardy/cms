from rest_framework import serializers
from main.models import Activity


class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = "__all__"


class ActivityDetailSerialzier(ActivitySerializer):
    depth = 2
