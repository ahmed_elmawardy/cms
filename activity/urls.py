from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import ActivityHandler


router = DefaultRouter()

router.register("list", ActivityHandler, basename="activity")
urlpatterns = [path("", include(router.urls))]
