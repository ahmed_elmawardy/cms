from main.models import Activity
from main.views import *
from .serializers import ActivitySerializer, ActivityDetailSerialzier


class InintActivity(GenericViewSet):
    queryseyt = Activity.objects.all()
    # permission_classes = (IsAuthenticated,)
    # authentication_class = (TokenAuthentication,)


class ActivityHandler(
    InintActivity,
    CreateModelMixin,
    ListModelMixin,
    UpdateModelMixin,
    RetrieveModelMixin,
):

    serializer_class = ActivitySerializer

    def get_serializer_class(self):
        if self.action == "retrieve":
            return ActivityDetailSerialzier

        return self.serializer_class
