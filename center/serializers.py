from rest_framework import serializers
from main.models import Center, Types


class CenterSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Center


class CenterDetailSerializer(serializers.ModelSerializer):
    types = serializers.StringRelatedField()

    class Meta:
        fields = ("id", "title", "faculty", "types", "service_set")
        model = CenterSerializer.Meta.model
        depth = 3


class CenterReportGeneratorSerializer(serializers.ModelSerializer):
    faculty = serializers.StringRelatedField()
    types = serializers.StringRelatedField()

    class Meta:
        fields = ("title", "faculty", "types")

        model = Center


class ComplexCentersReport(serializers.ModelSerializer):
    faculty = serializers.StringRelatedField()
    types = serializers.StringRelatedField()

    class Meta:
        model = Center

        fields = ("id", "title", "types", "faculty")
