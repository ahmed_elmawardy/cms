from django.urls import path, include
from .views import CenterHandler, CenterReportGeneratorView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

router.register("list", CenterHandler, basename="center")

router.register("reports", CenterReportGeneratorView, basename="center-report")

urlpatterns = [path("", include(router.urls))]
