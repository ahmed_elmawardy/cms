from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from main.models import Center, Plan, Report, Types
from main.views import *
from rest_framework.response import Response

from helpers.functions import render_to_pdf, _extract_cols_for

from .serializers import (
    CenterDetailSerializer,
    CenterReportGeneratorSerializer,
    CenterSerializer,
    ComplexCentersReport,
)


class InitCenter(GenericViewSet):
    queryset = Center.objects.all()
    # permission_classes = (IsAuthenticated,)
    # authentication_classes = (TokenAuthentication,)


class CenterHandler(
    InitCenter, ListModelMixin, CreateModelMixin, RetrieveModelMixin, UpdateModelMixin
):

    serializer_class = CenterSerializer

    def get_serializer_class(self):
        if self.action == "retrieve":
            return CenterDetailSerializer

        return self.serializer_class

    # def partial_update(self, request, *args, **kwargs):
    #     current_object = self.get_object()
    #     service = request.query_params.get('service')
    #     plan_type = request.query_params.get('plan_type')
    #     report_type = request.query_params.get('report_type')

    #     data = {
    #             'Done': False
    #         }

    #     if service:

    #         input(current_object.service_set.get(service))
    #     # if plan_type:
    #     #     feteched_type = get_object_or_404(Types.objects.all(), pk=plan_type)
    #     #     current_service.plan.types = None
    #     #     current_service.plan.types = feteched_type
    #     #     current_object.save()

    #     #     # input(type(current_service.plan.types))

    #     # if report_type:
    #     #     feteched_type = get_object_or_404(Types.objects.all(), pk=report_type)
    #     #     current_service.report.types = feteched_type

    #     return Response (status=200)


class CenterReportGeneratorView(InitCenter, ListModelMixin, RetrieveModelMixin):

    # serializer_class = CenterReportGeneratorSerializer
    serializer_class = ComplexCentersReport
    pdf_title = "Centers Report"
    pdf_app = "center"

    def get_pdf_headers(self):
        return self.serializer_class.Meta.fields

    def __init__(self, *args, **kwargs):
        self.pdf_headers = self.get_pdf_headers()

    def retrieve(self, request, *args, **kwargs):
        _obj = self.get_object()

        return render_to_pdf(
            context=_extract_cols_for([_obj], self.pdf_headers),
            headers=self.pdf_headers,
            title=self.pdf_title,
            app=self.pdf_app,
        )

    def list(self, request, *args, **kwargs):
        return render_to_pdf(
            context=_extract_cols_for(self.get_queryset(), self.pdf_headers),
            headers=self.pdf_headers,
            title=self.pdf_title,
            app=self.pdf_app,
        )
