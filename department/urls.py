from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import DepartmentHandler

router = DefaultRouter()
router.register("list", DepartmentHandler, basename="department")

urlpatterns = [path("", include(router.urls))]
