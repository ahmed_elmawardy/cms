from main.views import *
from main.models import Department
from .serializers import DepartmentSerializer, DepartmentDetailSerializer


class InitDepartment(GenericViewSet):
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

    queryset = Department.objects.all()


class DepartmentHandler(
    InitDepartment,
    ListModelMixin,
    RetrieveModelMixin,
    CreateModelMixin,
    UpdateModelMixin,
):

    serializer_class = DepartmentSerializer

    def get_serializer_class(self):
        if self.action == "retrieve":
            return DepartmentDetailSerializer

        return self.serializer_class
