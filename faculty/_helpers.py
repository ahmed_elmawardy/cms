from typing import Dict, Union, List

from django.http import HttpResponse
from django.template.loader import render_to_string, get_template
from rest_framework.status import HTTP_200_OK

from xhtml2pdf import pisa


def generat_pdf_report(faculty: Dict[str, Union[str, int, List]], html_path='faculty/report.html') -> "HttpResponse":
    response = HttpResponse(content_type='application/pdf; charset=\'utf-8\' ')
    # rendered_html = render_to_string('faculty/report.html', faculty)
    # pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), result, encoding='UTF-8')
    # pisa.CreatePDF(html.encode('UTF-8'), response, link_callback=fetch_resources, encoding='UTF-8')
    # result = BytesIO()

    # PDF
    # pdf = pisa.pisaDocument(BytesIO(html.encode("utf-8")), result)
    # if not pdf.err:
    #     return HttpResponse(result.getvalue(), content_type='application/pdf')

    # pdf = pisa.pisaDocument(cStringIO.StringIO(html.encode(a)).encode('utf-8'),file('mypdf.pdf','wb'))



    template = get_template(html_path)
    rendered_html = template.render(faculty)
    
    response['Content-Disposition'] = f'inline; filename=test.pdf'
    response['Accept-Charset'] = 'utf-8'
    response['status'] = HTTP_200_OK

    pisa.CreatePDF(rendered_html, dest=response, encoding='UTF-8',)

    return response