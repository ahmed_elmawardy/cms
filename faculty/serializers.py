from rest_framework import serializers
from main.models import Faculty


class FacultySerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Faculty


class FacultyDetailSerializer(FacultySerializer):
    # services = serializers.DictField()

    class Meta:
        fields = (
            "id",
            "name",
            # "services",
            "centers",
            "types",
        )
        model = FacultySerializer.Meta.model
        depth = 2


class FacultiesReportsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Faculty
        fields = "__all__"


class FacultyReportSerializer(serializers.ModelSerializer):
    all_services_no =  serializers.IntegerField(source='services_no')
    class Meta:
        model = Faculty
        
        fields = (
            "id",
            "name",
            # "services",
            "all_services_no",
            "center_services"
            )
    
    read_only_fields = [
        'all_services_no',
        'center_services',
        'id',
        'types'
    ]

    extra_kwargs = {
        'all_services_no' : {'read_only'},
        'center_services' : {'read_only'},
        'types' :{'read_only'},
        'code' :{'read_only'},
    }