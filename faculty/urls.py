from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import FacultyHandler, FacultyReports

router = DefaultRouter()
router.register("list", FacultyHandler, basename="faculty")
router.register("reports", FacultyReports, basename="faculty-reports")

urlpatterns = [path("", include(router.urls))]
