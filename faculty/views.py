from main.models import Faculty
from .serializers import (
    FacultySerializer,
    FacultyDetailSerializer,
    FacultiesReportsSerializer,
    FacultyReportSerializer,
)
from rest_framework.response import Response
from faculty._helpers import generat_pdf_report
from main.views import *

class InitFaculty(GenericViewSet):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated, )

    queryset = Faculty.objects.all()


class FacultyHandler(
    InitFaculty,
    ListModelMixin,
    RetrieveModelMixin,
    CreateModelMixin
    ):

    serializer_class = FacultySerializer

    def get_serializer_class(self):
        if self.action == "retrieve":
            return FacultyDetailSerializer

        return self.serializer_class


class FacultyReports(InitFaculty, RetrieveModelMixin, ListModelMixin):

    serializer_class = FacultiesReportsSerializer

    def get_serializer_class(self):
        if self.action == "retrieve":
            return FacultyReportSerializer

        return self.serializer_class
    
    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)

        # return response 
        return generat_pdf_report(response.data)


