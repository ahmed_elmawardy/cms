import uuid
from django.http import HttpResponse
from django.template.loader import render_to_string
from xhtml2pdf import pisa


def render_to_pdf(
    context: list,
    headers: list,
    title: str,
    app: str,
    enable_disposition=False,
    filename=None,
):

    if not filename:
        filename = str(uuid.uuid4().time_low)

    response = HttpResponse(content_type="application/pdf")

    if enable_disposition:
        response["Content-Disposition"] = f"attachment; filemane={filename}.pdf"

    context = {
        "title": title,
        "title_len": len(headers),
        "heades": headers,
        "context": context,
    }

    template = render_to_string(template_name=f"{app}/main.html", context=context)

    pisa.CreatePDF(template, dest=response)

    return response


def _extract_cols_for(objects: list, cols: list):
    returned_objects = []
    _obj = {}
    _obj_values = []

    for obj in objects:
        for col in cols:
            _obj_values.append(getattr(obj, col))
        returned_objects.append(_obj_values)
        _obj_values = []

    return returned_objects

