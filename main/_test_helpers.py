from django.urls import reverse

from main.models import (
    Center,
    Types,
    Plan,
    Report,
    Faculty,
    Service,
    Beneficiarie,
    Activity,
    Department,
)
from django.contrib.auth import get_user_model

types_payload = {"title": "monthly"}

beneficiarie = {"name": "namee", "gender": "male", "country": "egypt"}

beneficiarie_services = {"beneficiarie": 1, "services": 1}

report_payload = {"types": 1, "name": "asd"}

plan_payload = {
    "acadamic_year": "2021-2022",
    "title": "plan-1",
    "desc": "Desc",
    "types": 1,
}

user_payload = {
    "first": "Ali",
    "last": "Ali",
    "gender": 1,
    "address": "cairo",
    "state": "",
    "city": "cairo",
    "country": 1,
    "password": "123456",
    "email": "person@human.com",
    "phone": "01122456678",
}

activity_payload = {"name": "Activity", "desc": "asdasd"}

department_payload = {
    "name": "name",
    "faculty": 1,
    # 'users':1
}


faculty_payload = {"name": "Faculty"}

services_payload = {
    "plan": 1,
    "report": 1,
    "title": "serivce",
    "objectives": "Objectives",
}

center_payload = {"types": 1, "title": "Center", "faculty": 1, "service": 1}


def create_activity():
    return Activity.objects.create(**activity_payload)


def create_types(title=None):
    if title:
        types_payload.update({"title": title})

    return Types.objects.create(**types_payload)


def create_report():
    types = create_types()
    report_payload.update({"types": types})
    return Report.objects.create(**report_payload)


def create_paln():
    types = create_types()
    plan_payload.update({"types": types})
    return Plan.objects.create(**plan_payload)


def create_faculty():
    return Faculty.objects.create(**faculty_payload)


def create_serivce():
    services_payload.update({"plan": create_paln(), "report": create_report()})
    return Service.objects.create(**services_payload)


def create_center():
    faculty = create_faculty()
    service = create_serivce()
    types = create_types()
    center_payload.update({"faculty": faculty, "service": service, "types": types.id})

    return Center.objects.create(**center_payload)


def create_beneficiarie():
    return Beneficiarie.objects.create(**beneficiarie)


def create_user():
    return get_user_model().objects.create_normal_user(**user_payload)


def create_department():
    user = create_user()
    faculty = create_faculty()
    department_payload.update({"faculty": faculty})
    return Department.objects.create(**department_payload)


def add_app_url_(app: str) -> {}:
    _app = {}
    _app[app.upper()] = {
        "list": reverse(app + "-list"),
        "retrieve": reverse(app + "-detail", args=[1]),
    }

    return _app
