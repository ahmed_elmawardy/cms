from django.contrib import admin
from main.models import (
    Report,
    Center,
    Activity,
    Service,
    Beneficiarie,
    Plan,
    Types,
    Department,
    Faculty,
)


class FacultyAdmin(admin.ModelAdmin):
    list_display = ["name", "types"]

    fieldsets = ((None, {"fields": ("name", "types")}),)


# Register your models here.

admin.site.register(Report)
admin.site.register(Center)
admin.site.register(Activity)
admin.site.register(Service)


admin.site.register(Beneficiarie)
admin.site.register(Plan)
admin.site.register(Faculty, FacultyAdmin)
admin.site.register(Types)
admin.site.register(Department)
