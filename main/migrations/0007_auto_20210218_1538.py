# Generated by Django 3.1.6 on 2021-02-18 13:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [("main", "0006_auto_20210218_1502")]

    operations = [
        migrations.AddField(
            model_name="faculty",
            name="types",
            field=models.ForeignKey(
                default=1,
                on_delete=django.db.models.deletion.DO_NOTHING,
                to="main.types",
            ),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="faculty",
            name="name",
            field=models.CharField(max_length=50, verbose_name="Name"),
        ),
    ]
