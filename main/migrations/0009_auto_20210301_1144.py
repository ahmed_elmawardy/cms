# Generated by Django 3.1.6 on 2021-03-01 09:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("main", "0008_remove_report_types")]

    operations = [
        migrations.AlterField(
            model_name="beneficiarie",
            name="name",
            field=models.CharField(max_length=150, verbose_name="Beneficiarie name"),
        ),
        migrations.AlterField(
            model_name="beneficiarie",
            name="services",
            field=models.ManyToManyField(to="main.Service", verbose_name="Services"),
        ),
    ]
