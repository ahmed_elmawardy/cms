from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model


class Report(models.Model):
    name = models.CharField(_("Report"), max_length=50)

    # def centers(self):
    #     return self.center_set.get()

    def __str__(self):
        return str(self.name)


class Types(models.Model):
    title = models.CharField(_("Reporting type"), max_length=50)

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name_plural = "types"


class Beneficiarie(models.Model):
    genders = ((1, "Male"), (2, "Female"))
    country = ((1, "Egypt"), (2, "Germany"))

    name = models.CharField(_("Beneficiarie name"), max_length=150)
    gender = models.CharField(_("Gender"), choices=genders, max_length=50)
    country = models.CharField(_("Country"), choices=country, max_length=50)
    services = models.ManyToManyField("main.Service", verbose_name=_("Services"))

    def __str__(self):
        return str(self.name)


class Plan(models.Model):
    title = models.CharField(_("Plan title"), max_length=50)

    acadamic_year = models.CharField(_("Acadamic Year"), max_length=9)
    desc = models.TextField(_("Description"))
    types = models.ForeignKey(
        "main.Types", verbose_name=_("Plan Type"), on_delete=models.DO_NOTHING
    )

    def __str__(self):
        return str(self.title)


class Faculty(models.Model):
    name = models.CharField(_("Name"), max_length=50)
    types = models.ForeignKey("main.Types", on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name_plural = "faculties"
        ordering = ["-id"]

    def services(self):
        _inner_serivce = {}
        for center in self.center_set.all():
            for service in center.service_set.all():
                _inner_serivce[str(service)]  = service.objectives

        return _inner_serivce

    def services_no(self):
        return len(self.services())

    def center_services(self):
        _center_obj = {}
        for center in self.center_set.all():
            _center_obj[str(center)] = center.services()
        return _center_obj

        
    def centers(self):
        centers = []
        center_obj= {}
        for center in self.center_set.all():
            center_obj['id'] =  int(center.id)
            center_obj['title'] =  str(center)
            center_obj['types'] =  str(center.types)
            center_obj['services_no'] =  int(center.service_set.count())

            centers.append(center_obj)
            center_obj= {}
        return centers

    def __str__(self):
        return str(self.name)


class Department(models.Model):
    name = models.CharField(_("Department"), max_length=50)
    users = models.ManyToManyField(
        get_user_model(), verbose_name=_("users"), blank=True
    )

    faculty = models.ForeignKey(
        "main.Faculty", verbose_name=_("Faculty"), on_delete=models.DO_NOTHING
    )

    def __str__(self):
        return str(self.name)


class Activity(models.Model):
    name = models.CharField(_("Activity"), max_length=50)
    desc = models.TextField(_("Description"))

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name_plural = "activities"


class Service(models.Model):
    plan = models.ForeignKey(
        "main.Plan", verbose_name=_("Plans"), on_delete=models.DO_NOTHING
    )

    report = models.ForeignKey(
        "main.Report", verbose_name=_("Reports"), on_delete=models.DO_NOTHING
    )

    title = models.CharField(_("Service title"), max_length=50)
    objectives = models.TextField(_("Service objectives"))
    center = models.ForeignKey(
        "main.Center", verbose_name=_("Center"), on_delete=models.DO_NOTHING
    )

    def __str__(self):
        return str(self.title)


class Center(models.Model):
    class Meta:
        ordering = ["-id"]

    title = models.CharField(_("Center Title"), max_length=50)
    faculty = models.ForeignKey(
        "main.Faculty", verbose_name=_("Faculty"), on_delete=models.DO_NOTHING
    )

    types = models.ForeignKey("main.Types", on_delete=models.DO_NOTHING)

    def __str__(self):
        return str(self.title)

    def services(self):
        service_obj = {}
        services_container = []
        services = self.service_set

        for service in services.all():
            service_obj['id'] = int(service.id)  
            service_obj['title'] = str(service)  
            service_obj['objectives'] = str(service.objectives)  
            services_container.append(service_obj)

            service_obj = {}
        services_container.append({"services_no": len(services_container)})
        return services_container