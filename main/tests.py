from django.test import TestCase
from rest_framework.test import APIClient
from ._test_helpers import *
from rest_framework.status import (
    HTTP_200_OK,
    HTTP_204_NO_CONTENT,
    HTTP_201_CREATED,
    HTTP_405_METHOD_NOT_ALLOWED,
    HTTP_401_UNAUTHORIZED,
)


urls = {
    "REPORTS": {
        "retrieve": reverse("reports-detail", args=[1]),
        "list": reverse("reports-list"),
    }
}


class Faculty(TestCase):
    def setUp(self):
        return super().setUp()

    def test_retrieve_report(self):
        pass


# class Center(TestCase):
#     def setUp(self):
#         self.client = APIClient()
#         # installed_apps = [
#         #         'reports',
#         #         'types',
#         #         'faculty',
#         #         'department',
#         #         'activity',
#         #         'service'
#         #     ]
#         # for app in installed_apps:
#         #     urls.update(add_app_url_(app))

#     # Making sure that creates working well
#     def test_create_types(self):
#         types_instance = create_types()
#         self.assertEqual(types_instance.title, types_payload["title"])

#     def test_create_report(self):
#         report_instance = create_report()

#         self.assertIn(str(report_instance), report_payload["name"])

#     def test_create_plan(self):
#         paln_instance = create_paln()

#         self.assertEqual(str(paln_instance), plan_payload["title"])

#     def test_create_faculty(self):
#         faculty_instance = create_faculty()

#         self.assertEqual(str(faculty_instance), faculty_payload["name"])

#     # How client interacts with APIS
#     def test_list_reports(self):
#         report = create_report()
#         url = urls.get("REPORTS").get("list")
#         reports = self.client.get(path=url)

#         self.assertContains(reports, report.name, status_code=HTTP_200_OK)

#     def test_retrieve_report(self):
#         report_instnce = create_report()
#         url = urls.get("REPORTS").get("retrieve")
#         report = self.client.get(path=url)

#         self.assertContains(report, report_instnce.name, 1, HTTP_200_OK)

#     def test_destory_report(self):
#         report_instnce = create_report()
#         url = urls.get("REPORTS").get("retrieve")
#         report = self.client.delete(path=url)

#         self.assertEqual(report.status_code, HTTP_204_NO_CONTENT)
#         self.assertIsNone(report.data)

#     def test_create_report(self):
#         types = create_types()
#         create_url = urls.get("REPORTS").get("list")
#         response = self.client.post(
#             create_url, data={"name": report_payload["name"], "types": types.id}
#         )
#         self.assertEqual(response.status_code, HTTP_201_CREATED)

#     def test_types_create(self):
#         urls.update(add_app_url_("types_app"))
#         types = create_types()
#         url = urls.get("TYPES_APP").get("list")

#         response = self.client.post(path=url, data=types_payload)

#         self.assertEqual(response.status_code, HTTP_201_CREATED)

#     def test_create_plan(self):
#         types = create_types()
#         # plan_payload.update({'types':types.id})
#         # _paln = create_paln()
#         urls.update(add_app_url_("plan"))

#         response = self.client.post(
#             path=urls.get("PLAN").get("list"),
#             data={
#                 "acadamic_year": "2021-2022",
#                 "title": "plan-1",
#                 "desc": "Desc",
#                 "types": 1,
#             },
#         )

#         self.assertEqual(response.status_code, HTTP_201_CREATED)

#     def test_create_faculty(self):
#         faculty = create_faculty()
#         urls.update(add_app_url_("faculty"))
#         response = self.client.post(
#             urls.get("FACULTY").get("list"), data=faculty_payload
#         )

#         self.assertEqual(response.status_code, HTTP_201_CREATED)

#     def test_create_department(self):
#         department = create_department()
#         department_payload = {"name": "name", "faculty": 1}

#         urls.update(add_app_url_("department"))
#         url = urls.get("department".upper()).get("list")
#         response = self.client.post(url, department_payload)

#         self.assertEqual(response.status_code, HTTP_201_CREATED)

#     def test_create_activity(self):
#         activity = create_activity()
#         activity_payload = {"name": "Activity", "desc": "asdasd"}

#         urls.update(add_app_url_("activity"))
#         url = urls.get("activity".upper()).get("list")

#         response = self.client.post(url, activity_payload)

#         self.assertEqual(response.status_code, HTTP_201_CREATED)

#     def test_create_service(self):
#         service = create_serivce()

#         services_payloaded = {
#             "plan": 1,
#             "report": 1,
#             "title": "serivce",
#             "objectives": "Objectives",
#         }

#         plan = create_paln()
#         report = create_report()
#         urls.update(add_app_url_("service"))
#         url = urls.get("service".upper()).get("list")

#         response = self.client.post(path=url, data=services_payloaded)

#         self.assertEqual(response.status_code, HTTP_201_CREATED)

#     def test_create_center(self):
#         center = create_center()
#         path = "center"
#         urls.update(add_app_url_(path))
#         url = urls.get(path.upper()).get("list")
#         center_payload = {
#             'types':1,
#             "title": "Center",
#             "faculty": 1,
#             "service": 1}

#         create_serivce()
#         create_faculty()

#         response = self.client.post(path=url, data=center_payload)

#         self.assertEqual(response.status_code, HTTP_201_CREATED)

#     ### end create client

#     def test_patch_center(self):
#         center_str = "center"
#         center = create_center()
#         types_1 = create_types()
#         types_2 = create_types("annaul")
#         urls.update(add_app_url_(center_str))
#         url = urls.get(center_str.upper()).get("retrieve")
#         payload = {"types": types_2.id}

#         response = self.client.patch(url, data=payload)

#         self.assertContains(response, 4)
