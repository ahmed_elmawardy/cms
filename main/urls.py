from django.urls import include, path

urlpatterns = [
    path("reports/", include("reports.urls")),
    path("types/", include("types_app.urls")),
    path("plan/", include("plan.urls")),
    path("faculty/", include("faculty.urls")),
    path("department/", include("department.urls")),
    path("activity/", include("activity.urls")),
    path("service/", include("service_app.urls")),
    path("center/", include("center.urls")),
]
