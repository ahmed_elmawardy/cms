from rest_framework import serializers
from main.models import Plan


class PlanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Plan
        fields = "__all__"


class PlanDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Plan
        fields = "__all__"
