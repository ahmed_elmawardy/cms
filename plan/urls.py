from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import PlanHandler


router = DefaultRouter()

router.register("list", PlanHandler, basename="plan")

urlpatterns = [path("", include(router.urls))]
