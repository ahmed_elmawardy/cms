from main.views import *
from main.models import Plan
from .serializers import PlanSerializer, PlanDetailSerializer


class InitPlan(GenericViewSet):
    # authentication_classes =  (TokenAuthentication,)
    # permission_classes  =  (IsAuthenticated, )
    queryset = Plan.objects.all()


class PlanHandler(InitPlan, ListModelMixin, CreateModelMixin, RetrieveModelMixin):

    serializer_class = PlanSerializer

    def get_serializer_class(self):
        if self.action == "retrieve":
            return PlanDetailSerializer

        return self.serializer_class
