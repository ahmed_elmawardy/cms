from rest_framework import serializers
from .views import Report


class ReportsSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Report


class ReportDetialSerializer(ReportsSerializer):
    pass
