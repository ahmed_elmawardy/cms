from django.urls import include, path
from rest_framework.routers import DefaultRouter

from reports.views import ReportsHandler

router = DefaultRouter()
router.register("list", ReportsHandler, basename="reports")


urlpatterns = [path("", include(router.urls))]
