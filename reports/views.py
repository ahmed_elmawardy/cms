# from django.shortcuts import render
from main.models import Report
from main.views import *
from .serializers import ReportDetialSerializer, ReportsSerializer


class InitReporst(GenericViewSet):
    queryset = Report.objects.all()
    # permissions_class = (IsAdminUser, )
    # authentication_classes = (TokenAuthentication,)


class ReportsHandler(
    InitReporst,
    CreateModelMixin,
    ListModelMixin,
    RetrieveModelMixin,
    DestroyModelMixin,
    UpdateModelMixin,
):
    def get_serializer_class(self):
        if self.action == "retrieve":
            return ReportDetialSerializer

        return ReportsSerializer
