from rest_framework import serializers
from main.models import Service


class ServiceSerialzier(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Service


class ServiceDetialSerialzier(ServiceSerialzier):
    depth = 2


class ServiceEmbeddedSerialzier(serializers.ModelSerializer):
    plans = serializers.ListField()

    class Meta:
        model = Service
        fields = ("plans", "title", "objectives")
