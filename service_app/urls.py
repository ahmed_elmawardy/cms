from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import ServiceHandler


router = DefaultRouter()

router.register("list", ServiceHandler, basename="service")


urlpatterns = [path("", include(router.urls))]
