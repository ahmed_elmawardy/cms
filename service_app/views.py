from main.models import Service
from main.views import *
from .serializers import ServiceSerialzier, ServiceDetialSerialzier


class IntiService(GenericViewSet):
    queryset = Service.objects.all()
    # permission_classes      = (IsAuthenticated,)
    # authentication_classes  = (TokenAuthentication,)


class ServiceHandler(
    IntiService, ListModelMixin, UpdateModelMixin, CreateModelMixin, RetrieveModelMixin
):

    serializer_class = ServiceSerialzier

    def get_serializer_class(self):
        if self.action == "retrieve":
            return ServiceDetialSerialzier

        return self.serializer_class
