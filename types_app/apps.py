from django.apps import AppConfig


class TypesAppConfig(AppConfig):
    name = "types_app"
