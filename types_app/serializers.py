from rest_framework import serializers
from main.models import Types


class TypesSerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Types
