from django.urls import path, include
from .views import TypesHanlder
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register("list", TypesHanlder, basename="types_app")

urlpatterns = [path("", include(router.urls))]
