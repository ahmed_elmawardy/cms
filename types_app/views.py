from main.views import *
from main.models import Types
from types_app.serializers import TypesSerializer


class InitTypes(GenericViewSet):
    queryset = Types.objects.all()
    # permission_classes = (IsAuthenticated,)
    # authentication_classes = (TokenAuthentication,)


class TypesHanlder(InitTypes, ListModelMixin, CreateModelMixin, RetrieveModelMixin):

    serializer_class = TypesSerializer
