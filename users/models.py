from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin,
)
from django.db import models
from django.utils.translation import ugettext_lazy as _

REQUIRED_FIELDS = [
    "address",
    "city",
    "first",
    "last",
    "phone",
    "gender",
    "country",
    "password",
]

ALL_FIELDS = REQUIRED_FIELDS + ["state", "postal_code", "email"]


class CustomUsersManager(BaseUserManager):
    def validate_injected_keys(self, **kwargs: iter) -> {}:
        new_kwargs = {}

        for key in ALL_FIELDS:
            new_kwargs[key] = kwargs.get(key, 0)

        return new_kwargs

    def validate_required_fields(self, **kwargs):
        kwrags = self.validate_injected_keys(**kwargs)

        for key in kwrags:
            if kwrags[key] is None and key in REQUIRED_FIELDS:
                raise ValueError(key + ": couldn't be empty")

        return kwrags

    def create_normal_user(self, **kwargs):
        kwargs = self.validate_required_fields(**kwargs)

        password = kwargs.pop("password")
        user = self.model(email=self.normalize_email(kwargs.pop("email")), **kwargs)
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_staff(self, **kwargs):
        staff = self.create_normal_user(**kwargs)
        staff.is_staff = True
        staff.save(using=self._db)

        return staff

    def create_superuser(self, **kwargs):
        superuser = self.create_normal_user(**kwargs)
        superuser.is_superuser = True
        superuser.is_staff = True
        superuser.save()

        return superuser


class Users(AbstractBaseUser, PermissionsMixin):
    gender = (("male", "Male"), ("female", "Female"))

    email = models.EmailField(_("User's Email "), unique=True, max_length=255)
    address = models.CharField(_("Address"), max_length=150)
    city = models.CharField(_("City"), max_length=50)

    postal_code = models.IntegerField(_("Postal Code"), blank=True)

    state = models.CharField(_("State"), max_length=100, blank=True)

    first = models.CharField(_("First Name"), max_length=150)
    last = models.CharField(_("Last Name"), max_length=150)
    phone = models.CharField(_("Phone Number"), unique=True, max_length=20)
    gender = models.CharField(_("Gender"), choices=gender, default="male", max_length=6)

    country = models.CharField(_("Country"), max_length=50)

    is_staff = models.BooleanField(_("Staff"), default=False)

    is_superuser = models.BooleanField(_("Super Admin"), default=False)

    is_active = models.BooleanField(_("Is Active"), default=True)

    types = models.ForeignKey("main.Types", blank=True, on_delete=models.DO_NOTHING)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = REQUIRED_FIELDS

    objects = CustomUsersManager()
