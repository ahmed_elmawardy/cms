from django.test import TestCase
from rest_framework.test import APIClient
from django.contrib.auth import get_user_model


global_user = {
    "first": "Ahmed",
    "last": "Ali",
    "phone": "01122315546",
    "gender": "male",
    "city": "Cairo",
    "state": "",
    "postalcode": "",
    "country": "egypt",
    "password": "123456",
    "email": "ahmed@ali.com",
    "address": "Cairo",
}


class UsersTest(TestCase):
    def test_creat_normal_user(self):
        user = get_user_model().objects.create_normal_user(**global_user)

        self.assertEqual(str(user), global_user["email"])
        self.assertTrue(user.is_active)

    def test_creat_suerpuser(self):
        superuser = get_user_model().objects.create_superuser(**global_user)

        self.assertEqual(str(superuser), global_user["email"])
        self.assertTrue(superuser.is_active)
        self.assertTrue(superuser.is_superuser)
        self.assertTrue(superuser.is_staff)

    def test_create_staff(self):
        staff = get_user_model().objects.create_staff(**global_user)

        self.assertEqual(str(staff), global_user["email"])
        self.assertTrue(staff.is_active)
        self.assertFalse(staff.is_superuser)
        self.assertTrue(staff.is_staff)
